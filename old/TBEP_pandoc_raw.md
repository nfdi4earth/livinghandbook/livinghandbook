---
name: I am looking for data

description: Resources for finding available ESS data

author: 
  - name: Ivonne Anders
    orcidid: https://orcid.org/0000-0001-7337-3009
  - name: Sibylle K. Hassler
    orcidid: https://orcid.org/0000-0001-5411-8491
  - name: Marie Ryan
    orcidid: https://orcid.org/0000-0002-7890-9631

language: ENG

collection: 
  - TBEP_ENG.md 
  - Collection_ObserveCollectCreateEvaluate.md
  - Collection_StoreManage.md
  - Collection_SharePublish.md

subtype: topical_entry

subject: 
  - unesco:concept112
  - unesco:concept7387

keywords:
  - repository
  - archive
  - Earth System Science
  - research data management
  - data search
  - data publication
  - data
  - ownership

target_role: 
  - data collector
  - data owner
  - data depositor
  - data user
  - data curator
  - data librarian
  - data steward
  - data advocate
  - data provider
  - service provider
  - research software engineer
  - policy maker
  - general public

version: 1.0

license: CC-BY-4.0

---

# I am looking for data

## Resources for finding available ESS data 

Access to high-quality data is crucial for research. Therefore, we offer a wide range of resources and services to facilitate the search for suitable data. Currently, it is not yet possible to find data directly through our portal, but we are working on it! However, our guided search will help you to find the repository or archive that might have the data you are looking for. Are you already familiar with standards, licences and repositories? Then start with the complete list of available data sources.

A number of questions arise around data search and use. What is a repository? Who owns the data and how are you actually allowed to use the data? How is the quality of data assessed or evaluated? There are many data formats in earth system sciences. Each discipline may have its own standards and requirements. Maybe you want to use the data in the cloud? There are various tools that support this. 

## Read the articles and information we recommend on these and other topics.

:::: {.cols style="display: flex; background-color: #d3d3d3;"}

::: {.column}
[![Who owns the data?](img/who_owns_the_data.png "Who owns the data? Data ownership and licenses")](WhoOwnsTheData_ENG.md)    

:::

::: {.column}

[![What are repositories (in ESS)?](img/what_are_repositories.png "What are repositories (in ESS)?")](WhatAreRepositories_ENG.md)

:::

::: {.column}

<!--Unclear why the following links direct the reader to _under_construction.md. There is no information given about the respective topics.-->

[![Data quality in Earth System Science?](img/data_quality_in_ess.png "Data quality in Earth System Science")](_under_construction.md)

:::

::: {.column}

[![Data formats in Earth System Science](img/data_formats_in_ess.png "Data formats in Earth System Science")](_under_construction.md)

:::

::: {.column}

[![Tools for cloud solutions](img/tools_for_cloud_solutions.png "Tools for cloud solutions")](_under_construction.md)
:::

::::

