---
name: Reusability of data with complex semantic structure

description: Description of the NFDI4Earth Pilot project. 

author: 
  - name: Anne Strack
    orcidId: https://orcid.org/0000-0003-4748-528X
  - name: Lukas Jonkers
    orcidId: https://orcid.org/0000-0002-0253-2639
  - name: Robert Huber
  - name: Michal Kucera

inLanguage: ENG

about:
  - external nfdi4earth
  - data service
  - metadata
  - tools and techniques	


isPartOf: 
  - N4E_Pilots.md
  - Collection_AccessReuse.md

additionalType: technical_note

subjectArea: 
  - dfgfo:314-01
  - unesco:concept4045
  - unesco:concept4011
  - unesco:concept162
  - unesco:concept7387
  - unesco:concept16291
  - unesco:concept13753

keywords:
  - PANGAEA
  - World Register of Marine Species
  - data harmonisation
  - foraminifera
  - community guidelines

audience: 
  - data user
  - data curator

version: 1.0

identifier: https://doi.org/10.5281/zenodo.8124211


license: CC-BY-4.0

---

# Reusability of data with complex semantic structure

Data on the occurrence and abundance of fossils provide invaluable insights into past climate and biodiversity change. However, lack of common taxonomic standards and associated vocabularies, limit reusability of fossil data and thus global assessments. Inconsistent and variable taxonomy are a common challenge faced in biodiversity research using species occurrence data. This pilot aimed to resolve those semantic barriers for the example of planktonic foraminifera. We designed and developed an R workflow ([Media 1](#med1)) that applies the resolved semantics on legacy data stored in [PANGAEA](https://www.pangaea.de/) while making use of [WoRMS](https://www.marinespecies.org/) (World Register of Marine Species). Furthermore, we provide community guidelines for new data submissions of species abundance data to generate sustainable ways of combining legacy and new data. As the pilot is closely linked to PANGAEA, we expect that many users will benefit from our workflows and best practice solutions. Since heterogeneous data structures and inadequate ontology support are a common problem for any other geoscientific and biodiversity research communities, we hope that our approach can be transferred on different types of long-tail data. 

<a id="med1"></a>

![Simplified cross-functional flowchart sketching the functioning of the
R script to harmonize planktonic foraminifera abundance data.](img/Pilot_ReusabilityData_media1.jpg "Simplified cross-functional flowchart sketching the functioning of the
R script to harmonize planktonic foraminifera abundance data.")

## Resources
* Roadmap / project report ([@strack_2023])
* Poster 1 ([Zenodo](https://doi.org/10.5281/zenodo.8123959))
* Poster 2 ([Zenodo](https://doi.org/10.5281/zenodo.8123935))
* R script ([Zenodo](https://doi.org/10.5281/zenodo.8124240) | [Github](https://github.com/lukasjonkers/harmonisePFTaxonomy))

## References

