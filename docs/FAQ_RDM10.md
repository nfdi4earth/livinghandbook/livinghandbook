---
name:  How do I decide which data I should keep and archive and which I can delete?

author: 
  - name: User Support Network

inLanguage: ENG

about:
  - internal nfdi4earth
  - evaluation
  


isPartOf: 
  - FAQ.md 

additionalType: FAQ

subjectArea: 
  - unesco:concept3117 
  - unesco:concept2768

keywords:
  - data archiving
  - data disposal

audience: 
  - data owner
  - data depositor
  - data steward
  - data curator

version: 1.0

license: CC-BY-4.0

---

#  How do I decide which data I should keep and archive and which I can delete?

Over time and also during a project, data is generated - measurement data, raw data, analysis data, result data. However, storage space is often limited, and archiving data is not always practical. Deleting data can be a challenging task. The decision tree linked [here](keep-or-delete_EN.md) can help determine which data should be retained and which can be deleted.
