---
name:  Observe, collect, create and evaluate

description: Collection of articles about research data management topics related to the organisation and procedures of data acquisition and the collection of datasets.

author:
  - name: NFDI4Earth Editorial Board
#    orcidId: http://orcid.org/0000-0002-8186-3566

inLanguage: ENG

additionalType: collection

subjectArea:
  - dfgfo:34
  - unesco:mt2.35
  - unesco:concept7387
  - unesco:7396
  - unesco:4711

keywords:
  - research data management
  - data acquisition
  - data collection
  - data organisation
  - data structure

audience:
  - data collector
  - data owner
  - data depositor
  - data user
  - data curator
  - data librarian
  - data steward
  - data advocate
  - data provider
  - service provider
  - research software engineer
  - policy maker
  - general public

version: 1.0

license: CC-BY-4.0

---

# Observe, Collect, Create, and Evaluate

## What is the observe, collect, create, and evaluate phase?

After the data requirements and architecture are defined through the 1st phase – [Plan and Design](Collection_PlanDesign.md) –, the workflow continues with the 2nd phase of the data lifecycle **Observe, Collect, Create and Evaluate**.

**Observe**:  This is rather qualitative, e.g., describing or sketching a rock formation, taking images of city landscapes or videos of ocean floors, recording sounds, or categorising wind strengths by observing its consequences on nature.

**Collect**: This involves mostly quantitative measurements such as vegetation covering using remote sensing, atmospheric temperature profiles with weather balloons, earthquakes with seismometers, or chemical compositions with analytical machines. However, collect also comprises the collection of samples in the field for later analytical data collection in the laboratory. The result is a systematic and structured collection of information from various sources.

**Create**: The aim here is to create new, often experimental or model-based data, e.g., testing self-driving cars, model climate predictions, track changes in a mineral subjected to high temperature and pressure, or study tsunami formation in a water tank.

**Evaluate**: The requirements outlined in the [Plan and Design](Collection_PlanDesign.md) phase are applied to assess the validity and quality of the acquired data during observe/collect/create and used as a guide to convert these acquired data into a [FAIR](FAQ_RDM2.md) dataset. This ensures the data are interoperable and can be used together with existing data, including from other fields, and studied in respective and wider contexts. For example, analytical field data can be easily plotted with previous field data, or directly imported in existing geoscience models to test their influence.

## Why is the observe, collect, create, and evaluate phase important?
A reliable database is the foundation of all scientific research to reliably address a research question. It ensures the accuracy, validity and reproducibility of results, and minimizes bias and errors.  After a successful implementation of this 2nd step in the data lifecycle, the 3rd is [Store and Organise](Collection_StoreManage.md), in which insights are derived.

## What will be offered in this article collection?
Methods and techniques for data observation, collection, and creation, as well as appropriate methods for evaluating the database focused on Earth System Science disciplines. These resources support researchers in conducting data-driven research and promote scientific accuracy and reliability as well as innovation and progress in Earth System Sciences.

We encourage contributions from the community to help refine and enhance this collection and provide general or domain specific strategies for more effective and collaborative data solutions.  

For more details on how to contribute, please refer to our [template article](example_ENG.md).