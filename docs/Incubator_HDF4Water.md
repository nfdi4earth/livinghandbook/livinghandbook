---
name: Hierarchical Data Format for Water-related Big Geodata (HDF4Water)

description: Description of the NFDI4Earth Incubator project. 

author: 
  - name: Hao Li
    orcidId: https://orcid.org/0000-0002-6336-8772
  - name: Martin Werner
    orcidId: https://orcid.org/0000-0002-6951-8022

inLanguage: ENG

about:
  - external nfdi4earth
  - dataset
  - tools and techniques


isPartOf: 
  - N4E_Incubators.md
  - Collection_PlanDesign.md

additionalType: article

subjectArea: 
  - dfgfo:315-02
  - unesco:mt2.35
  - unesco:concept3269
  - unesco:concept3052
  - unesco:concept7398

keywords:
  - Big Data
  - Geospatial Aritificial Intelligence
  - Hierarchical Data Format

audience: 
  - data depositor
  - data librarian
  - data steward
  - data provider

identifier: https://doi.org/10.5281/zenodo.7562587

version: 1.0

license: CC-BY-4.0

---

# Hierarchical Data Format for Water-related Big Geodata (HDF4Water)

<a id="med1"></a>

![Flowchart of the operations carried out to map data from OSM into a HDF5 container losslessly](img/Incubator_HDF4Water_media1.png "Flowchart of the operations carried out to map data from OSM into a HDF5 container losslessly.")

&nbsp;

## Abstract
Humans rely on clean water for their health, well-being, and various socio-economic activities. To ensure an accurate, up-to-date map of surface water bodies, the often heterogeneous big geodata (remote sensing, GIS, and climate data) must be jointly explored in an efficient and effective manner. In this context, a cross-platform and rock-solid data representation system is key to support advanced water-related research using cutting-edge data science technologies, like deep learning (DL) and high-performance computing (HPC). In this incubator project, we will develop a novel data representation system based on Hierarchical Data Format (HDF), which supports the integration of heterogeneous water-related big geodata and the training of state-of-the-art DL methods. The project will deliver high-quality technical guidelines together with an example water-related data repository based on HDF5 with the support of the Big Geospatial Data Management group at the Technical Unviersity of Munich, with which the NFDI4Earth will consistently benefit from this incubator project since the solution can serve as a blueprint for many other research fields facing the same big data challenge. 

## Outcomes and Trends
The novelty and contribution of this incubator project can be summarized mainly as follows:

An efficient file format concept: preference was given to HDF5 data container as we require:

1. Fast access (memory mapping),
2. Built-in support of compression,
3. Multilingual strings (UTF-8),
4. Platform-independence,
5. Command line tool support,
6. Hierarchical structuring into groups or folders,
7. Single file for downloads and DOI assignments.

An analysis-ready representation of OSM: OSM data is a key ingredient in our machine learning
scheme, and it is known for its unique and simple data representation. Therefore, we designed an
analysis-ready mapping by triangulating multi-polygons and embedding an R*-tree into HDF5.  
  
A dedicated C++ implementation and Python module: we followed the FAIR principle during the
development of AtlasHDF. As a result, the source code in C++ and Python, the openly-available
data (e.g., OSM and Sentinel-2), together with a tutorial in Jupyter Notebook as Open Educational
Resources (OERs) were made available in GitHub with a DOI. 

## Resources

* Code repository ([Github](https://github.com/tum-bgd/atlashdf) | [Zenodo](https://doi.org/10.5281/zenodo.7562587))
* [Promotional Video](https://www.youtube-nocookie.com/embed/GIzH6io7tTk)
* [Science Talk](https://www.youtube-nocookie.com/embed/duSZxZdHE04)
* [Final Report](https://git.rwth-aachen.de/nfdi4earth/pilotsincubatorlab/incubator/hdf4water/-/blob/master/Project_Description_HDF4Water.pdf)
* [Tutorial](https://www.youtube-nocookie.com/embed/kUAg2cvA3v4)
* [Container](https://github.com/tum-bgd/atlashdf/blob/main/Dockerfile)
