---
name: Data Processing and Analysis

description: Collection of articles about research data management topics related to process and analyse (processing, analyse) research data.

author: 
  - name: NFDI4Earth Editorial Board
 #   orcidId: http://orcid.org/0000-0002-8186-3566

inLanguage: ENG

additionalType: collection

subjectArea: 
  - dfgfo:34
  - unesco:mt2.35
  - unesco:concept522
  - unesco:concept2214

keywords:
  - research data management
  - data processing
  - data postprocessing
  - data analysis

audience: 
  - data collector
  - data owner
  - data depositor
  - data user
  - data curator
  - data librarian
  - data steward
  - data advocate
  - data provider
  - service provider
  - research software engineer
  - policy maker
  - general public

version: 1.0

license: CC-BY-4.0

---

# Process and Analyse

## What is the process and analyse phase?

The stored raw data from the 3rd phase [Organise and Store](Collection_StoreManage.md) need to be cleaned up and structured in the 4th phase of the data lifecycle to prepare the data for extracting information and insights.

**Process**: The aim of data processing is to prepare the previously organised raw data for analysis. Algorithms, software, or computational techniques might be used during data processing, e.g., for noise filtering of seismic data, formatting of geospatial data from different sources, or preparing geochemical datasets for machine learning.

**Analyse**: Data analysis extracts insights and patterns from processed data. Compared to data processing, data analysis focuses on answering the research questions. Statistical, mathematical, or computational techniques might be used, to, e.g., identify potential oil or gas reservoirs through seismic and geospatial analysis or tracking deforestation using remote sensing.

## Why is the process and analyse phase important?

This step transforms raw data into structured information and is the step in the data lifecycle where data is used to gain new insights and scientific knowledge to e.g., enable accurate resource exploration, environmental monitoring, or disaster prediction. Such new insights and interpretations need to be systematically [described](Collection_Describe.md), which is done in the next step of the data lifecycle.

## What will be offered in this article collection?

Processing and analyzing data relies almost exclusively on digital tools. Ever more algorithms, software and computational techniques are developed and used for research in Earth System Science (ESS). This collection contains a number of such software and tools, as well as information how to produce or where to find such tools and software..

We encourage contributions from the ESS community to share the software or tools that they develop or use to help their research or optimise the data processing workflow. Experience about how they use these techniques to solve scientific questions are also welcomed. 

For more details on how to contribute, please refer to our [template article](example_ENG.md).