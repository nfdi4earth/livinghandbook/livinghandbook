---
name: Plan and Design

description: Collection of articles about research data management topics related to the plan and design stage. 

author: 
  - name: NFDI4Earth Editorial Board
#    orcidId: https://orcid.org/0000-0002-0709-5044

inLanguage: ENG

additionalType: collection

subjectArea: 
  - dfgfo:34
  - unesco:mt2.35
  - unesco:concept522

keywords:
  - research data management
  - data management
  - plan
  - design

audience: 
  - data collector
  - data owner
  - data depositor
  - data user
  - data curator
  - data librarian
  - data steward
  - data advocate
  - data provider
  - service provider
  - research software engineer
  - policy maker
  - general public

version: 1.0

license: CC-BY-4.0

---

# Plan and Design

## What is the plan and design phase?

It is the 1st phase of the data lifecycle. The goal is to come up with a plan for creating the desired dataset that follows the [FAIR principles](FAQ_RDM2.md). Planning and designing are closely linked, but have different focuses.

**Plan**: planning involves strategic decisions to define the requirements of e.g., who is responsible for the data management, or what methods to use, e.g., analytical instruments for rock compositions, surveys for city development, experiments to understand ocean currents, or observational studies of agricultural developments. It is then established what domain specific data service, data commons, or domain repository is mandated and/or applicable. The requirements to ensure data quality and integrity are defined. The results are documented in a Data Management Plan (DMP) for which tools such as a Research Data Management Organiser (RDMO) might be used.  

**Design**: designing focuses on the technical and operational details. For example, it determines what metadata standards, vocabularies, or naming conventions are most applicable, e.g., based on community recommendations or best established and used. Guidelines for collecting and managing research data, including informed consent, confidentiality, or data security measures are selected.
 
## Why is the plan and design phase important?

It lays the groundwork and architecture for all subsequent steps along the data lifecycle and ensures efficient, secure, and effective data handling. Proper planning and designing provide a clear roadmap and guide for decisions related to data collection, storage, processing, and usage. A careful completion of the 1st phase of the data lifecycle ensures the effectiveness of the 2nd phase [Observe, Collect, Create, and Evaluate](Collection_ObserveCollectCreateEvaluate.md).  

## What will be offered in this article collection?

Guidance on setting objectives, defining data requirements, and designing infrastructure to support Earth science data management. Descriptions of ESS relevant data services, data commons and related topics are preferred in this collection.

We encourage contributions from the community to help refine and enhance this collection and provide general or domain specific strategies for more effective and collaborative data solutions.  

For more details on how to contribute, please refer to our [template article](example_ENG.md).