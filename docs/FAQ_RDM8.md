---
name: What is a DOI? Or a PID?

author: 
  - name: User Support Network

inLanguage: ENG

about:
  - internal nfdi4earth
  - publication
  - sharing data


isPartOf: 
  - FAQ.md 

additionalType: FAQ

subjectArea: 
  - unesco:concept3117 
  - unesco:concept3412

keywords:
  - persistent identifiers
  - findability
  - data retrieval

audience: 
  - data user
  - general public
  - data depositor
  - data steward
  - data librarian

version: 1.0

license: CC-BY-4.0

---

# What is a DOI? Or a PID?

A DOI (Digital Object Identifier) and a PID (Persistent Identifier) are both standardized ways to uniquely identify and locate digital resources, such as articles, datasets, books, or any other online content. They play a crucial role in ensuring the long-term accessibility and citability of these resources. Since URLs as location tend to change over time for whatever reason they are obviously inappropriate as stable reference. The abstraction layer of PID can be seen as a pointer to the location and is a necessary precondition for any stable and sustainable referencing in the web. DOI (Digital Object Identifier): A DOI is a unique alphanumeric string assigned to a digital object, such as a research article, dataset, or publication. DOIs are managed by registration agencies and are associated with metadata that describe the resource, such as its title, author(s), publication date, and more. This makes it easy for researchers and readers to locate and cite the resource accurately.

PID (Persistent Identifier): A PID is a broader term that encompasses various types of identifiers, including DOIs. PIDs are used to uniquely identify digital objects and ensure their long-term accessibility. There exist a varity of PID systems such as [DOI](https://www.doi.org/), [handle](https://handle.net/index.html), [ARK](https://arks.org/about/), [arXiv](https://arxiv.org/), to name a few of them (see for example [here](https://sis.web.cern.ch/submit-and-publish/persistent-identifiers/pids-for-objects) for a more detailed overview). These identifiers are designed to be "persistent," meaning they are meant to remain functional and valid over time, even if the location of the digital object changes. PIDs are used across various domains, including research, data management, cultural heritage, and more.

In summary, a DOI is a specific type of PID, commonly used in scholarly publishing, to uniquely identify and provide a permanent link to digital resources. PIDs, on the other hand, are a broader category of identifiers that encompass various schemes used to ensure the persistence and discoverability of digital objects. 
