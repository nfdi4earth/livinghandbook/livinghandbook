---
name: Spatial Data Science across Languages (SDSL) Workshop 2023

description: In September 2023, a first-of-its-kind workshop was held at the Institute of Geoinformatics, University of Münster to foster a discussion between the different software developer communities in the three scripting languages of R, Python and Julia dominating the Spatial Data Science sphere. 


author: 
  - name: Yomna Eid
    orcidId: https://orcid.org/0000-0001-7713-9682
  - name: Edzer Pebesma
    orcidId: https://orcid.org/0000-0001-8049-7069

inLanguage: ENG


about:
  - internal nfdi4earth
  - resources

isPartOf: 
  - Success Stories
  - LHB_ENG.md
  - Collection1.md
  - Collection2.md


additionalType: recommended_article


subjectArea: 
  - dfgfo:315-02
  - dfgfo:409-02
  - unesco:concept160
  - unesco:mt2.35

keywords:
  - R
  - Python
  - Julia
  - Spatial Data Science
  - Workshop
  - Success Story
  - Measure 2.5
  - Measure 1.3

audience: 
  - research software engineer
  - data user
  - data advocate
  - general public


identifier: 

version: 0.1

license: CC-BY-4.0

---

# Spatial Data Science across Languages (SDSL) Workshop 2023

_**Contributions to the community**: This work started a cross-community conversation about standardizing the implementations of spatial data science research software. The workshop has been taken up as an international initiative, and already a second iteration of the event is planned. A growing community is exchanging information on this topic on a dedicated channel._
<p>&nbsp;</p>

![Workshop Logo](img/SDSL_thumbnail.png "Workshop focused on R, Python, and Julia")

Spatial data science is a field that arises from bottom-up, in and from many existing scientific disciplines and industrial activities. Earth system scientists (ESS) all use spatio-temporal data in one form or another. As such, the importance of finding answers to spatial questions based on available data, and communicating that effort often using scripting languages is pivotal within the ESS community.

However, the history and the goals of the languages R, Python and Julia, as well as the differences between the communities and their lack of communication, has led to differences between the APIs and implementations found across the three languages. We attempt to bring these three communities together in our two-day Spatial Data Science across Languages (SDSL) workshop to look at commonalities in their approaches (the use of C++ libraries), as well as their differences.

Hot topics such as vector data cubes, emerging vector data formats such as GeoArrow, and spatial support were discussed. A more extensive list of the main session topics is available at the [workshop homepage](https://r-spatial.org/sdsl/).

The workshop was conducted as a hybrid event with a total of 43 participants: 25 of whom were in-person, while the rest participated in the discussion streamed via Zoom. The participants were also not limited to software developers but users, students, as well as researchers. An evaluation form of the SDSL workshop with a participation rate of 81% reveals an overall rating of 94% satisfaction.

## Impact
This work started a cross-community conversation about standardizing the implementations of spatial data science research software. The workshop has been taken up as an international initiative, and a second iteration of the event will be held in Prague, in 2024. A growing community is exchanging information on this topic on a dedicated channel on [Discord](https://discord.com/channels/1153662760605995122/1153662761298038846)

## Additional information
* [Announcement for SDSL Workshop 2024](https://spatial-data-science.github.io/2024/)
* [Blog post about the workshop by Martin Fleischmann (participant)](https://martinfleischmann.net/a-note-on-spatial-data-science-across-languages-vol.1/)
* [Blog post about the workshop by Josiah Parry (participant)](https://josiahparry.com/posts/2023-09-20-sdsl/)

