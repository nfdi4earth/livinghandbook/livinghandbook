---
name:  What are metadata, and why is it important to add metadata to datasets?

author: 
  - name: User Support Network

inLanguage: ENG

about:
  - internal nfdi4earth
  - metadata


isPartOf: 
  - FAQ.md 

additionalType: FAQ

subjectArea: 
  - unesco:concept3117 
  - unesco:concept3799

keywords:
  - data description
  - data documentation

audience: 
  - data collector
  - data owner

version: 1.0

license: CC-BY-4.0

---

#  What are metadata, and why is it important to add metadata to datasets?

Metadata provides context and information about the data itself which is essential to understanding, managing and utilizing data effectively. Typically, metadata is a set of attributes or properties that offer insights into the content, structure and characteristics of a dataset, document, file or any other information resource. Metadata standards differ among industries and diciplines and they evolve over time along with the progression of technology and research fields. A well-known type of metadata is e.g. geospatial metadata which is used to store data in a geographic information system (GIS). 
