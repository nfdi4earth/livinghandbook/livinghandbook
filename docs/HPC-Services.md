---
name: Overview on NFDI4Earth operated High Performance Computing service instances

description: An overview of services for High Performance Computing (HPC) operated by members of the NFDI4Earth. 

author: 
  - name: IG HPC
    orcidId: NN

inLanguage: ENG


about:
  - internal nfdi4earth
  - resources
  - data service
  - tools and techniques

isPartOf: 
  - Collection_ProcessAnalyse.md

additionalType: article

subjectArea: 
  - dfgfo:34
  - unesco:concept7387
  - unesco:mt2.35
  - unesco:concept17017
  - unesco:concept84
  - unesco:concept11831
  - unesco:concept7291
  

keywords:
  - High performance computing
  - HPC
  - processing

version: 0.1

license: CC-BY-4.0

---

# Overview on NFDI4Earth operated High Performance Computing service instances

The following table provides an overview about services for High Performance Computing (HPC) operated by members of the NFDI4Earth. The [NFDI4Earth HPC IG](IG-HPC.md) collected the initial list of services. 

The list aims to provide an overview of HPC competencies. Please find more information on how to apply for HPC resources within the NFDI4Earth project [here](HPC-Resources_Application.md).

<!-- URL: Please provide a URL to the English description --> 
<!-- Access: Please briefly describe what is needed to get access to the HPC service. You can add a URL for more detailed information. -->
<!-- Contact: Please provide a mail address in the best case. -->
<!-- Additional info: Please describe relevant information for potential users that is not covered in the previous columns. -->

| Name | Institution | Web | Access | Contact | Additional information | 
|----|----|----|----|----|-----|
| Albedo | Alfred-Wegener-Institut | [AWI homepage](https://www.awi.de/en/science/special-groups/scientific-computing/high-performance-computing.html) | institutional resource | - | Tier-3-system |
| Alpha Centauri, Barnard | CIDS ZIH - Information Services and High Performance Computing, TU Dresden | [CIDS homepage](https://tu-dresden.de/zih/hochleistungsrechnen?set_language=en#intro) | institutional resource, available for NFDI4Earth projects | [helpdesk@nfdi4earth.de](mailto:helpdesk@nfdi4earth.de) | [information about application](https://git.rwth-aachen.de/nfdi4earth/livinghandbook/livinghandbook/-/blob/main/docs/HPC-Resources_Application.md) |
| Cray XC40 | Deutscher Wetterdienst | [DWD homepage](https://www.dwd.de/EN/aboutus/it/it_node.html;jsessionid=5972AE5796E0FCD8A3F31243D1FA5429.live31094) | tbd | tbd | tbd |
| Mistral | Deutsches Klimarechenzentrum | [DKRZ homepage](https://www.dkrz.de/en/systems/hpc/hlre-4-levante?set_language=en) | tbd | tbd | tbd |
| Linux Cluster | Leibniz Supercomputing Centre of the Bavarian Academy of Sciences and Humanities (Leibniz-Rechenzentrum der BAdW, LRZ) | [LRZ homepage](https://doku.lrz.de/linux-cluster-10745672.html/) | Scienctists from Bavarian Universities and the BAdW have access (usually via their IT services; otherwise via LRZ servicedesk), including collaborators (e.g. in the scope of NFDI4Earth). | <https://servicedesk.lrz.de>; within NFDI4Earth: [rdm@lists.lrz.de](mailto:rdm@lists.lrz.de) | Heterogeneous cluster of HPC systems in the PFlops range and some special machines; also includes LRZ AI systems (GPU-based). |
| LRZ Compute Cloud | Leibniz Supercomputing Centre of the Bavarian Academy of Sciences and Humanities (Leibniz-Rechenzentrum der BAdW, LRZ) | [LRZ homepage](https://doku.lrz.de/compute-cloud-10333232.html/) | Access can be requested by German research-oriented organisations (paid access) or given by the Master User within the scope of collaborative projects; access to SuperMUC-NG implies free access to the SuperMUC-NG Cloud part. | <https://servicedesk.lrz.de>; within NFDI4Earth: [rdm@lists.lrz.de](mailto:rdm@lists.lrz.de) | NOT really a HPC system, but an IaaS Cloud based on OpenStack; e.g. used in pre- and postprocessing for HPC. |
| SuperMUC-NG | Leibniz Supercomputing Centre of the Bavarian Academy of Sciences and Humanities (Leibniz-Rechenzentrum der BAdW, LRZ) | [LRZ homepage](https://doku.lrz.de/hoechstleistungsrechner-10333235.html/) | Access via compute-time grants won by a competitive scientific-technological proposal to GCS (test access with reduced proposal possible), cf. <https://doku.lrz.de/application-for-a-project-on-supermuc-ng-11482823.html/>  | <https://servicedesk.lrz.de>; within NFDI4Earth: [rdm@lists.lrz.de](mailto:rdm@lists.lrz.de) | Highest-performance machine of LRZ, one of the three German &quot;Höchstleistungsrechner&quot; in the &quot;Gauss Centre for Supercomputing&quot; (GCS). | 

This article is part of the NFDI4Earth Living Handbook. Do you miss an HPC service? Please add it here: <https://git.rwth-aachen.de/nfdi4earth/livinghandbook/livinghandbook/-/edit/main/docs/HPC-Services.md?ref_type=heads>.
<!--

If you contribute to the article, please don't forget to add you as author and to sign the author agreeement (https://nfdi4earth.pages.rwth-aachen.de/livinghandbook/livinghandbook/#LHB_author-guidelines_ENG/#author-agreement-and-licences). 

-->
