---
name: Criteria for expert checks of articles
description: List of criteria to guide the decision whether an article submitted to the NFDI4Earth Living Handbook should be checked by an external expert.

author: 
  - name: Thomas Rose
    orcidId: http://orcid.org/0000-0002-8186-3566

inLanguage: ENG

about:
  - internal nfdi4earth
  - workflow


isPartOf: LHB_ENG.md

additionalType: article

subjectArea: 
  - unesco:concept3810
  - unesco:concept326
  - unesco:concept4224

keywords:
  - Living Handbook 
  - NFDI4Earth

audience: 
  - general public

version: 1.0

license: CC-BY-4.0

---

# Criteria for expert checks of articles

An article needs to be checed by an external expert only if one or more of the criteria listed below apply. If none of them apply, the article does not need to be checked by an external expert. 

* The editor does not identify themselves as expert of the article's topic 
    * The author of the article is not a member of the project or affiliated to the institution they are writing about
    * The editor is unsure whether the author is an expert on the topic they are writing about and accessible information about them does not provide sufficient clarity
* The author requests a check of the article's content. 

