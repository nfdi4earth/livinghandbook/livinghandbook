---
name: What is NFDI4Earth?

author: 
  - name: User Support Network

inLanguage: ENG

about:
  - internal nfdi4earth

isPartOf: 
  - FAQ.md 

additionalType: FAQ

subjectArea: 
  - unesco:concept3117 
  - dfgfo:34
  - unesco:mt2.35

keywords:
  - NFDI4Earth

audience: 
  - general public

version: 1.0

license: CC-BY-4.0

---

# What is NFDI4Earth?

[NFDI4Earth](https://onestop4all.nfdi4earth.de/) is a consortium within Germany's National Research Data Infrastructure (NFDI) initiative, dedicated to Earth System Sciences. It focuses on creating a sustainable infrastructure for managing, sharing, and reusing research data in this field. By developing tools, services, and methods, NFDI4Earth aims to make Earth System Science data more accessible, interoperable, and in line with the FAIR (Findable, Accessible, Interoperable, Reusable) principles.

For more information, you can explore the [NFDI4Earth collection](NFDI4Earth.md) of Living Handbook articles, check out how to participate [pilots and incubator](Participate_PilotsIncubator.md), [interest group](Participate_IG.md), and [acedemy](Participate_Academy.md).