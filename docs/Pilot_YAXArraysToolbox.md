---
name: Empowering Earth System Science through Julia for Optimized Processing and Statistical Driver Attribution in Big Data

description: Description of the NFDI4Earth Pilot project. 

author: 
  - name: Daniel E. Pabon-Moreno
    orcidId: https://orcid.org/0000-0003-4058-7339 

inLanguage: ENG

about:
  - external nfdi4earth
  - dataset
  - tools and techniques
  
isPartOf: 
  - N4E_Pilots.md
  - Collection_ProcessAnalyse.md

additionalType: technical_note

subjectArea: 
  - dfgfo:315-02
  - unesco:concept1557
  - unesco:concept2214
  - unesco:concept17105

keywords:
  - Julia
  - Biometeorology
  - Spatio-temporal analysis

audience: 
  - data user
  - service provider

version: 1.0

identifier: https://doi.org/10.5281/zenodo.7995187


license: CC-BY-4.0

---

# Empowering Earth System Science through Julia for Optimized Processing and Statistical Driver Attribution in Big Data

The availability of remote sensing information of the Earth has increased exponentially over the last decades. This trend is expected to continue as new remote sensing products from new satellite missions become available. To keep the pace with the amount of information gathered by the various sensors orbiting the Earth, it is essential to develop tools that allow scientists and students to easily manipulate and perform operations on spatio-temporal gridded data. Ideally, the new generation tools should be able to run, and interact with data on cloud platforms rather than individual computers. In this pilot, we developed a new Julia package as an entrance point to high-performance computing based on the YAXArrays.jl package, the YAXArraysToolbox package [@DOI:10.5281-zenodo.7989936]. The front-end of the YAXArraysToolbox package is divided in two modules, the first one (Basic Operations, [Media 1](#fig1)) contains a set of basic operations to process and visualize large gridded data in a very efficient way. The second module (Spatio-Temporal Analyses, [Media 2](#fig2)) contains a set of tools to perform data-driven attribution, e.g., using the space-for-time concept, and spatio-temporal data partitioning for cross-validation analyses based on [@DOI:10.1038-s41467-017-02810-8], [@DOI:10.1016-j.envsoft.2017.12.001], and [@DOI:10.1111-2041-210X.13650]. We anticipate that the package will be beneficial to various domains within the Earth system data science, and Julia user communities by providing a user-friendly environment. Advanced users will also benefit by taking advantage of the implemented analyses to perform data-driven attribution using machine learning techniques and semi-empirical modeling. We intend to further enhance the capabilities of the package by incorporating additional functions useful for statistical driver attribution in large datasets, including forward feature selection based on regularized regression.

<a id="fig1"></a>

![YAXArraysToolbox package. Basic operations module. Structure and functions.](img/Pilot_YAXArraysToolbox_fig1.png "YAXArraysToolbox package. Basic operations module. Structure and functions.") 

<a id="fig2"></a> 

![YAXArraysToolbox package. Spatio-temporal analysis module. Structure and functions.](img/Pilot_YAXArraysToolbox_fig2.png "YAXArraysToolbox package. Spatio-temporal analysis module.")

## Resources

* [Roadmap](https://zenodo.org/record/7995187)
* Code repository ([Zenodo](https://zenodo.org/record/7989936) | [Github](https://github.com/dpabon/YAXArraysToolbox.jl))
* JupyterNotebooks ([Zenodo](https://zenodo.org/record/7992014) | [Github]( https://github.com/dpabon/YAXArraysToolboxNotebooks))

## References
