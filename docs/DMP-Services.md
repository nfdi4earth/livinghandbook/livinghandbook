---
name: Overview on Data Management Plans service instances

description: An overview of services for Data Management Plans operated by members of the NFDI4Earth and beyond. 

author: 
  - name: Claudia Müller
    orcidId: https://orcid.org/0000-0002-0709-5044	 
  - name: Thomas Rose
    orcidId: https://orcid.org/0000-0002-8186-3566

inLanguage: ENG

about:
  - internal nfdi4earth
  - data management plans 
  - data service


isPartOf: 
  - Collection_PlanDesign.md

additionalType: article

subjectArea: 
  - dfgfo:34
  - unesco:concept7387
  - unesco:mt2.35

keywords:
  - Data management plan
  - DMP
  - research data management

audience: 
  - data owner
  - data collector
  - data steward

version: 0.1

license: CC-BY-4.0

---

# Overview on Data Management Plans service instances

If you need general information about Data Managment Plans (DMPs), we recommend the paper ["A Vision for Data Management Plans"](https://zenodo.org/records/10570654)
The following table provides an overview about services for Data Management Plans (DMPs) operated by members of the NFDI4Earth and beyond. The provided information is manually collected from the services. This overview is constantly growing.

| Instance | URL | Institution | Access | Contact | Additional information | Consulting |
|----|----|----|----|----|-----|-----|
| dmponline | <https://dmponline.dcc.ac.uk/> | Digital Curation Centre | Login with institutional credentials, or via registration | <dmponline@dcc.ac.uk> | To create, review, and share data management plans that meet institutional and funder requirements; public DMP examples for many subject areas are available; also recommended by national universities, like University of Heidelberg | data@uni-heidelberg.de |
| GFBio Data Management Plan | <https://dmp.gfbio.org/> | GFBio | Free account, only necessary to save dynamic DMP | <info@gfbio.org> | main audience: Life Sciences |
| Goethe-RDMO | <https://rdmo.uni-frankfurt.de/> | Goethe-Universität Frankfurt | only for institutional members | <rdmo@rz.uni-frankfurt.de> | different DMP templates available |
| HeFDI Data Management Plans and Research Data Management Organiser (RDMO) | <https://www.uni-marburg.de/en/hefdi/hefdi-data-services/hefdi-data-management-plans-rdmo> | HeFDI Data Services | With a few exceptions, most of the listed RDMOs are only accessible to members of these institutions. The RDMO of TU Darmstadt is an exception, as it is accessible via the ORCID account | <hefdi@uni-marburg.de> | Mostly available to HeFDI – Hessian Research Data Infrastructures members |
| TUB-DMP | <https://dmp.tu-berlin.de/> | Technische Universität Berlin | Login with institutional credentials | <team@dmp.tu-berlin.de> | maintain and share data management plans for your research projects |
| TUdmo | <https://tudmo.ulb.tu-darmstadt.de/> | TU Darmstadt | Login with ORCID, and through Helmholtz AAI via institutional login, GitHub and Google | <tudata@tu-darmstadt.de> | different DMP templates, template for software management plan |

<!--
This row can be used for copy-pasting (replace 'Placeholder' with content): 

| Placeholder | <Placeholder> | Placeholder | Placeholder | <Placeholder> | Placeholder |

If you contribute to the article, please don't forget to add you as author and to sign the author agreeement (https://nfdi4earth.pages.rwth-aachen.de/livinghandbook/livinghandbook/#LHB_author-guidelines_ENG/#author-agreement-and-licences). 

-->
