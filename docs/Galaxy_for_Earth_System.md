---
name: "Galaxy for Earth System"

description: Galaxy now supports Earth sciences, including climate, ecology, and environmental research. With its open-source platform and commitment to FAIR principles, Galaxy provides a powerful set of tools for building, sharing, and replicating workflows. From novice to expert, researchers can access user-friendly tools for comprehensive data analysis without programming skills. Join a growing community leveraging advanced technologies and collaborative resources to drive insights into our planet’s systems—perfect for Earth data enthusiasts looking to elevate their research.

author:  
  - name: Marie Jossé 
    orcidId: https://orcid.org/0009-0008-0622-604X 
  - name: Jérôme Detoc  
    orcidId: https://orcid.org/0009-0009-1320-8349

inLanguage: ENG

about:
  - internal nfdi4earth
  - tools and techniques

isPartOf: 
  - Collection_ProcessAnalyse.md
  - Collection_tools_pilots_incubators.md

subjectArea: 

keywords:
  - data analysis
  - web processing
  - software
  - tools

audience: 
  - general public 


subjectArea: 
  - dfgfo:409-02
  - unesco:concept6081 


version: 1.0

license: CC-BY-4.0

---

# Galaxy for Earth System

Galaxy was first launched in 2005 for bioinformatics. Since then, the platform has greatly improved and expanded its scientific domains. Thanks to the wide community of researchers, developers and administrators supporting Galaxy, the platform now can also assist researchers who have a scientific focus on the Earth system, environment, climate, ecology, and more.

This service is completely open source and follows the FAIR principles. Galaxy offers a large set of tools (computational scripts) from the various domains mentioned earlier. These tools can be chained to create analytical workflows, which can be runned, adapted, published and shared. Each step in an analysis is automatically stored in a history, allowing researchers to replicate this analysis.
This greatly supports collaborative work amongst scientists allowing them to share tools and analyses, thereby preventing redundant efforts and fostering efficiency. Furthermore, both novice and experienced users can easily access Galaxy as it is designed to provide a web user-friendly interface that requires no programming skills.
Galaxy integrates advanced technologies, including HPC scheduling (such as Slurm), the ability to run containers (such as dockers) or packages (such as Conda packages), and offers interactive environments like JupyterLab and RStudio.

As previously mentioned, the Galaxy community is growing and expanding to new domains, including the Earth system. It offers extensive training materials and workshops to help users, especially beginners, to allow them to make the most out of this platform.

One dedicated interface with tools and workflows for earth data analyses can be accessed [here](https://earth-system.usegalaxy.eu/).

Whether you are analysing geosphere, biosphere, hydrosphere, and atmosphere, in short all Earth related sciences, this is the place to be!

![Galaxy for Earth System](img/Galaxy_for_Earth_System_fig1.png "Galaxy for Earth System")

## What is available?

### Tools to access and process data 

Tools for earth data analysis are freely available in the ToolShed which can be installed on any Galaxy server.
Some tools available:
* [JupyterLab for Copernicus Data Space Ecosystem](https://earth-system.usegalaxy.eu/root?tool_id=interactive_tool_copernicus_notebook)
* [Argo data access tool](https://earth-system.usegalaxy.eu/root?tool_id=toolshed.g2.bx.psu.edu/repos/ecology/argo_getdata/argo_getdata/0.1.15+galaxy0)
* [Geographical Information System QGIS](https://earth-system.usegalaxy.eu/root?tool_id=interactive_tool_qgis)
* [Pangeo’s tools](https://earth-system.usegalaxy.eu/root?tool_id=interactive_tool_pangeo_notebook)

If tools are missing or information is not up-to-date in the list, please help us! 
Contact: [Marie](mailto:marie14.josse@gmail.fr) or [Jérôme](mailto:jerome.detoc@ifremer.fr) about it.

### Workflows and tutorials
Several curated Galaxy workflows are publicly available for different kinds of earth data analysis. Many of these are accompanied by comprehensive [Galaxy Training Network (GTN) Tutorials](https://training.galaxyproject.org/training-material/search2?query=earth-system) that will guide you through the analysis step by step.

Some examples of workflows and analysis:

* [Oceanographic studies](https://training.galaxyproject.org/training-material/topics/climate/tutorials/ocean-variables/tutorial.html)
* [Biogeochemical and physical ocean study](https://training.galaxyproject.org/training-material/topics/climate/tutorials/argo_pangeo/tutorial.html)
* [Volcanic eruption atmosphere study](https://training.galaxyproject.org/training-material/topics/climate/tutorials/sentinel5_data/tutorial.html)

![Galaxy for Earth System - Process](img/Galaxy_for_Earth_System_fig2.png "Galaxy for Earth System - Process")

Want to include your workflow here? Let us know! (contact [Marie](mailto:marie14.josse@gmail.fr) or [Jérôme](mailto:jerome.detoc@ifremer.fr))

### Training material and events

The Galaxy community organizes regular [training events](https://galaxyproject.org/events/). 

Below is the project involving members of this community:
* [EOSC FAIR-EASE](https://fairease.eu/): A new approach to observation and modeling of the Earth System, Environment and Biodiversity 

### Join us
Anybody interested in earth data analysis in Galaxy is welcome to join the earth galaxy community! Everybody is Welcome!
