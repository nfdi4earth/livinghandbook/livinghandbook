---
name: Tools from NFDI4Earth pilot and incubator projects
# The title of the article.

description: Collection of tools and applications which were developed and used in NFDI4Earth pilot and incubator projects
# A brief summary of the article, max. 300 characters incl. spaces.

author: 
  - name: Felix Cremer 
    orcidId: https://orcid.org/0000-0001-8659-4361
  - name: Fabian Gans
    orcidId: https://orcid.org/0000-0001-9614-0435
# The author(s) of the article. The name must be given, the ORCID should be given. As many authors as necessary can be listed. 

inLanguage: ENG
# The language of the article. Accepted values are:  
# ENG for English
# DEU for German

isPartOf: 
  - LHB_ENG.md
  - Collection_ProcessAnalyse.md
# The filenames of the NFDI4Earth Living Handbook collection(s), the article belongs to, as they appear in https://git.rwth-aachen.de/nfdi4earth/livinghandbook/livinghandbook/-/tree/main/docs. 
# Alternatively, the titles of the collections. The editor will replace it with the collections' file names. 

additionalType: article
# The type of the article. Acceptable terms are: 
# article: The most generic category. Should only be used if none of the others fit. 
# best_practice: A procedure of how a defined problem described in the article can be solved which is a (de-facto) standard or widely accepted in the community
# workflow: A step-by-step description of a procedure
# technical_note: A short article presenting a new software tool, method, technology. 
# recommended_article: a short review of a publication.
# discussion/review: An article that summarises/compares or (critically) discusses e.g., the state of understanding, different approaches or methodologies of a topic. 
# user_guide: Manuals on how to use e.g., software products. 
# collection: A NFDI4Earth Living Handbook collection article.
# FAQ: Can only used by the NFDI4Earth User Support Network, which define the FAQs. 
# topical_entry: Can only used by the NFDI4Earth Living Handbook Editorial Board. Identifies the articles featured in the topical entry points. 
# call_participate: Can only used by the NFDI4Earth Living Handbook Editorial Board. Identifies the articles featured in the "How to participate" section on of the OS4A Portal start page. 

#subject: 

# A list of controlled terms describing the subject of the article, given as their identifiers. Acceptable are any terms from: 
# DFG subject ontology: https://terminology.tib.eu/ts/ontologies/dfgfo/terms
# UNESCO thesaurus: https://vocabularies.unesco.org/browser/thesaurus/en/

keywords:
  - tool
  - software  
  - data analysis
  - NFDI4Earth
  - pilot
  - incubator
  
# A list of terms to describe the article's topic more precisely. 

audience: 
  - data user
# A list of the main roles of persons within research data management that is addressed by the article. 
# data collector: The person responsible for collecting, generating, re-using or contributing to research data and documents contexts and processes that relates to data handling. 
# data owner: Typically the principal researcher of the project or research group leader who is responsible for establishing who has access to the data, and reducing or mitigating the risk of any mishaps; e.g. improper storage facility, non-compliance with specific guidelines. 
# data depositor: Either the data creator or data owner and is the person who will perform the deposit of the data to a data preservation facility, like a repository or data archive. They perform activities such as uploading data after it has been generated, making it suitable for archiving, creating metadata and data documentation and submitting the data to the facility, so it can be preserved for the long-term. 
# data user: Someone who is interested in accessing and interacting with data for verification or (re)using data to evaluate, analyse, model and generally handle them to create insights and knowledge. 
# data curator: A person who ensures availability and reliability of data. They review and harmonise metadata of datasets submitted for storage and ensure that they are generally compliant with relevant policies before publication. They further monitor access to the data, support data owners in fulfilling data access requests and possess deeper technical knowledge about the backend of the repository or storage system. 
# data librarian: An information professional responsible for developing technical expertise on practical solutions of data management, archiving and dissemination and on data mining and visualization. 
# data steward: A supporter who advises researchers (or otherwise involved) persons on data management, privacy and information security aspects and can understand the meaning and applications of the data. They support: data creators on quality assurance, storage, policy compliance; data depositors on data selection, metadata and data documentation creation, preserving and publishing; data custodians on data curation, access controls, FAIR data management 
# data advocate: A person promoting the proper use of research data and technology and providing assistance or guidance in doing so. 
# data provider: A person or organization that stores and provides datasets to others.
# service provider: An organization that provides services related to the processing and management of data, such as, high-performance computing facilities or web-based maps.
# research software engineer: A person working with researchers to gain an understanding of the problems they face, and then develops, maintains and extends software to provide the answers. 
# policy maker: In research data management, policy makers strive to design effective, technology-neutral, forward-looking and coherent data governance policies across sectors.
# general public: All persons that do not belong to any of the other groups. 

identifier: 
# If the article has already a unique identifier such as an DOI, provide it here as URL (e.g., https://doi.org/10.1001/12345). 

version: 0.1
# The version of the article; will be updated by the editor. 

license: CC-BY-4.0
# The license of the article. Must not be altered. 

---

# Tools, software and applications from NFDI4Earth pilot and incubator projects

This article gives an overview of tools, software and workflows that were developed and applied within the [pilot](N4E_Pilots.md) and [incubator](N4E_Incubators.md) projects of NFDI4Earth. Where possible, the tools are directly linked including notebooks and tutorials. 

## List of tools and software from pilots and incubators

### YAXArraysToolbox.jl

YAXArraysToolbox is a software package to efficiently perform basic data aggregation tasks on gridded data.

- [tutorials for YAXArrayToolbox](https://github.com/dpabon/YAXArraysToolboxNotebooks)
- [tool description](Pilot_YAXArraysToolbox.md)

### MetBase

MetBase is the largest meteorite database for chemical and iostopic data and has been improved in the scope of the NFDI4Earth Metbase Pilot.

- [pilot and tool description](Pilot_Metbase.md)

### EcoSound

EcoSound provides tools for standardized access to passive acoustic monitoring data. It was developed in the PAMBase pilot

- [pilot and tool description](Pilot_PAMBase.md).

### Hydrograpr
Hydrographr is a 
It was developd in the GeoFRESH pilot.

- [pilot and tool description](Pilot_GeoFRESH.md)
- [tool repository](https://github.com/glowabio/hydrographr)
- [geoFresh repository](https://github.com/glowabio/geofresh)

### ESMValtool 
ESMValtool is a software to compare different Earth system models. It was further developed in the scope of the ESMValtool pilot.

- [tool description](https://esmvaltool.org) 
- [pilot description](Pilot_ESMValtool.md)
- [tutoria](https://tutorial.esmvaltool.org/) 

### scrAIber 

Scraiber is a web tool for automatic segmentation of olivine crystals in back scatter electron images using deep learning. 
It has been developed in the scope of the scrAIber incubator project 

- [tool repository](https://git.rwth-aachen.de/nfdi4earth/pilotsincubatorlab/incubator/scraiber)
- [examples for scrAIber](https://git.rwth-aachen.de/nfdi4earth/pilotsincubatorlab/incubator/scraiber/-/tree/master/scraiber/examples)
- [project description](Incubator_scrAIber.md)

### Aquatic Ecosystem

Aquatic Ecosystems has been developed in the scope of the Aquatic Ecosystems incubator project.

- [software project](https://git.rwth-aachen.de/nfdi4earth/pilotsincubatorlab/incubator/aquatic-ecosystems/-/tree/master/notebooks)
- [project description](Incubator_AquaticEcosystem.md)








