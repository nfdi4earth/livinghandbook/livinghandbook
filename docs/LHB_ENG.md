---
name: The NFDI4Earth Living Handbook documentation

description: A collection of articles related to the NFDI4Earth Living Handbook.

author: 
  - name: Thomas Rose
    orcidId: http://orcid.org/0000-0002-8186-3566

inLanguage: ENG

additionalType: collection

isPartOf: 
  - index.md

subjectArea: 
  - unesco:mt2.35
  - unesco:concept8079
  - unesco:concept503

keywords:
  - NFDI4Earth
  - Living Handbook

audience: 
  - general public

version: 1.0

license: CC-BY-4.0

---

# The NFDI4Earth Living Handbook documentation

To curate the content of the [NFDI4Earth Living Handbook](index.md), an editorial board was established. Its primary tasks are supporting authors in publishing their articles in the NFDI4Earth Living Handbook and to develop its content further. 

This set of articles provides guidance for authors, documents the editorial workflow and other features of the NFDI4Earth Living Handbook and decisions taken by the NFDI4Earth Living Handbook Editorial Board. 
