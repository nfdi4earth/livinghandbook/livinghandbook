---
name: Frequently asked questions (FAQ)

description: Collection of the FAQs, provided by the User Support Network

author: 
  - name: User Support Network

inLanguage: ENG

additionalType: collection

subjectArea: 
  - dfgfo:313-02
  - unesco:concept3117
  - unesco:mt2.35

keywords:
  - support

audience: 
  - data collector
  - policy maker
  - data owner
  - data user
  - data librarian
  - data steward
  - data advocate
  - data provider
  - service provider
  - research software engineer

version: 1.0

license: CC-BY-4.0

---

# Frequently asked questions (FAQ)

Please find below a list of all FAQs compiled by the NFDI4Earth User Support Network. 

## Research Data Management

* [How can I publish my data?](FAQ_RDM1.md)
* [What are the FAIR principles?](FAQ_RDM2.md)
* [What is a research data management plan?](FAQ_RDM3.md)
* [How do I cite research data correctly?](FAQ_RDM4.md)
* [Why does publishing data in FAIR repositories take so long compared to Zenodo?](FAQ_RDM5.md)
* [What license should I use for publishing my data?](FAQ_RDM6.md)
* [What are metadata, and why is it important to add metadata to datasets?](FAQ_RDM7.md)
* [What is a DOI? Or a PID?](FAQ_RDM8.md)
* [How can I find a suitable repository for my data?](FAQ_RDM9.md)
* [How do I decide which data I should keep and archive and which I can delete?](FAQ_RDM10.md)
* [What is the difference between storing, publishing and long-term storage of/archiving research data?](FAQ_RDM11.md)

## NFDI4Earth

* [What is NFDI4Earth?](FAQ_NFDI1.md)
* [What does the NFDI4Earth offer?](FAQ_NFDI2.md)
* [How can I contribute to the NFDI4Earth?](FAQ_NFDI3.md)
* [Where does the content in the NFDI4Earth come from?](FAQ_NFDI4.md)
