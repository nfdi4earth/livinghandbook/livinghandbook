---
name: NFDI4Earth Software Architecture

description: The NFDI4Earth software architecture documentation summarizes the major user cases, gives an overview on the requirement analysis approaches for the main NFDI4Earth software components, and points out envisioned quality goals and potential architecture’s stakeholders.

author: 
    - name: Christin Henzen	
      orcidId: https://orcid.org/0000-0002-5181-4368
    - name: Anna Brauer
      orcidId: https://orcid.org/0000-0002-7092-1492 	
    - name: Auriol Degbelo
      orcidId: https://orcid.org/0000-0001-5087-8776
    - name: Stephan Frickenhaus
      orcidId: https://orcid.org/0000-0002-0356-9791
    - name: Jonas Grieb	
      orcidId: https://orcid.org/0000-0002-8876-1722 
    - name: Stephan Hachinger
      orcidId: https://orcid.org/0000-0001-8341-1478
    - name: Ralf Klammer
      orcidId: https://orcid.org/0000-0003-4816-6440
    - name: Claudia Müller
      orcidId: https://orcid.org/0000-0002-0709-5044
    - name: Johannes Munke
      orcidId: https://orcid.org/0000-0002-5031-9170
    - name: Tom Niers
      orcidId: https://orcid.org/0000-0002-5746-8590
    - name: Daniel Nüst
      orcidId: https://orcid.org/0000-0002-0024-5046
    - name: Claus Weiland
      orcidId: https://orcid.org/0000-0003-0351-6523
    - name: Alexander Wellmann
      orcidId: https://orcid.org/0000-0002-9033-5538

inLanguage: ENG

about:
  - internal nfdi4earth


isPartOf: 

additionalType: article

subjectArea:  

keywords: 
- software architecture
- system architecture
- Knowledge Hub
- OneStop4All
- EduTrain
- Living Handbook
- User Support Network

version: 0.1

license: CC-BY-4.0

---
# NFDI4Earth System Architecture Documentation

Here we provide the documentation of the NFDI4Earth software architecture. The documentation summarizes the major use cases, gives an overview on the requirement analysis approaches for the NFDI4Earth services, points out envisioned quality goals, and introduces potential stakeholders of the architecture. Specifically, the documentation provides externally and internally driven constraints that need to be considered when developing or contributing to NFDI4Earth services. We herewith facilitate, the users to better understand the solution strategy for the NFDI4Earth architecture. 
 
You can read the current version online on the NFDI4Earth GitLab: [https://nfdi4earth.pages.rwth-aachen.de/architecture/architecture-docs/](https://nfdi4earth.pages.rwth-aachen.de/architecture/architecture-docs/) 
<br>
or directly jump to specific topics:
- [General NFDI4Earth Architecture Constraints](https://nfdi4earth.pages.rwth-aachen.de/architecture/architecture-docs/#architecture-constraints), e.g., using free and open source software
- [Community services](https://nfdi4earth.pages.rwth-aachen.de/architecture/architecture-docs/#community-services)
- [NFDI4Earth-developed Services Knowledge Hub, OneStop4All, User Support Network, EduTrain and Living Handbook](https://nfdi4earth.pages.rwth-aachen.de/architecture/architecture-docs/#nfdi4earth-developed-services) 
- relevant [NFDI Basic Services](https://nfdi4earth.pages.rwth-aachen.de/architecture/architecture-docs/#nfdi-basic-services)
- [Deployment details for our distributed services](https://nfdi4earth.pages.rwth-aachen.de/architecture/architecture-docs/#deployment-view)
- [Architecture decision process and decisions](https://nfdi4earth.pages.rwth-aachen.de/architecture/architecture-docs/#architecture-decisions)
- [NFDI4Earth Architecture Team](https://www.nfdi4earth.de/2coordinate/software-architecture-team)

![NFDI4Earth Architecture](img/NFDI4Earth_Architecture.JPG "NFDI4Earth Architecture")

The architecture documentation will be updated regularly.

The second version is available on the NFDI4Earth Zenodo community: <br> 
Henzen, C., Brauer, A., Degbelo, A., Frickenhaus, S., Grieb, J., Hachinger, S., Klammer, R., Müller, C., Munke, J., Niers, T., Nüst, D., Weiland, C., & Wellmann, A. (2024). NFDI4Earth Software Architecture Documentation. Zenodo. https://doi.org/10.5281/zenodo.14534839

Contact: [Christin Henzen](mailto:christin.henzen@tu-dresden.de) or the [NFDI4Earth Architecture Team](mailto:nfdi4arth-architecture@tu-dresden.de) with your questions and comments.
