---
name: Data journals - peer-review publications about data 

description: Article that describes what data journals are and where to find them

author: 
  - name: Ivonne Anders
    orcidId: https://orcid.org/0000-0001-7337-3009
  - name: Sibylle K. Hassler
    orcidId: https://orcid.org/0000-0001-5411-8491
  - name: Marie Ryan
    orcidId: https://orcid.org/0000-0002-7890-9631

inLanguage: ENG

about:
  - internal nfdi4earth
  - data service
  - publication

isPartOf: 
  - Collection_SharePublish.md

additionalType: article

subjectArea: 
  - unesco:concept2753
  - unesco:mt2.35
  - dfgfo:34

keywords:
  - data 
  - journal
  - research data management
  - data search
  - data publication

audience: 
  - data collector
  - data owner
  - data depositor
  - data user
  - data curator
  - data librarian
  - data steward
  - data advocate
  - data provider
  - service provider
  - research software engineer
  - policy maker
  - general public

version: 1.0

license: CC-BY-4.0

---

# Data journals - peer-review publications about data 

(adapted from the corresponding [article](https://forschungsdaten.info/themen/veroeffentlichen-und-archivieren/datenjournale/) on [forschungsdaten.info](https://forschungsdaten.info)) 

## What are data journals?

In addition to the traditional scientific journals for articles describing and interpreting research results, there are also data journals in which articles are published that only describe data but do not interpret it. Such articles describe, for example, data sets that are particularly significant or comprehensive or that are highly complex. The particular advantage of these data descriptions is that they undergo a peer review process similar to research articles in traditional journals and therefore meet a high quality standard.

The data described in the article are only linked to the article and are published separately, ideally in a [data repository](WhatAreRepositories_ENG.md). Of course, it is also important to provide a link (ideally including [DOI](FAQ_RDM8.md)) to the data journal article in the repository in which the data was published.

In this way, a data publication in a data repository can include different articles: an article in a data journal (detailed description, without interpretation) and a research article in a classic scientific journal (focusing on the interpretation of the data).

Like traditional journals, data journals are either open access journals or closed access journals. Some data journals use open peer reviews for quality assurance in order to improve the transparency of the review process. One example of this from Earth System Sciences is the open access data journal [Earth System Science Data](https://www.earth-system-science-data.net/).

Closely related to data journals are journals that publish descriptions of software, e.g. [The Journal of Open Research Software](https://openresearchsoftware.metajnl.com/) or the [Journal of Statistical Software](https://www.jstatsoft.org/index).

## Where can I look for data journals?

Data journals can be found, for example, via the [list on forschungsdaten.org](https://www.forschungsdaten.org/index.php/Data_Journals). A [list sorted by research subject](https://www.uni-wuerzburg.de/en/rdm/information/data-publication/) of the University of Würzburg also provides references to some data journals. A further list with approx. 130 data journals can be found [here](https://www.uni-wuerzburg.de/rdm/informationen/datenpublikation/).


