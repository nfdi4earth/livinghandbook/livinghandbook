---
name: Suggest your service

description: NFDI4Earth aims to share information on high-quality services that support FAIR and open research data management in the Earth System Sciences. Here, we provide an overview of steps to suggest your service.

author:
  - name: Christin Henzen
    orcidId: https://orcid.org/0000-0002-5181-4368

inLanguage: ENG

about:
  - internal nfdi4earth  

isPartOf:
  - Collection_ObserveCollectCreateEvaluate.md

additionalType: article

subjectArea:

keywords:
  - metadata
  - services

version: 0.1

license: CC-BY-4.0
---

# Suggest your service

NFDI4Earth is a community-driven initiative aiming to provide researchers with FAIR, coherent, and open access to relevant Earth System resources, such as services.

In NFDI4Earth, we follow the service definition used in the joint statement of NFDI consortia on basic services (https://doi.org/10.5281/zenodo.6091657): A service in NFDI is understood as a technical-organisational solution, which typically includes storage and computing services, software, processes, and workflows, as well as the necessary personnel support for different service desks.

Offering high-quality community services as resources is key to supporting the researchers. To suggest a service, you can follow these steps:

## 1. Understand the NFDI4Earth goals and requirements

* __[Mission](https://www.nfdi4earth.de/about-us)__: Familiarize yourself with the mission and goals of NFDI4Earth. This will help you to ensure that your service aligns with our objectives.
* __[Software architecture documentation](https://nfdi4earth.pages.rwth-aachen.de/architecture/architecture-docs/)__: Review the software architecture documentation, particularly the requirements and architecture constraints. 

## 2. Tell us more about your service by preparing the following information

* __Service description__: Clearly describe the service you are proposing. Include its purpose, key features, and how it will benefit the NFDI4Earth community.
* __Target audience__: Identify who will benefit from your service, such as specific research groups, or the broader scientific community.
* __Technical specifications__: Provide technical details about the service.

## 3. Contact the NFDI4Earth Helpdesk

* Please provide your questions or interest and service information to the NFDI4Earth Helpdesk: [helpdesk@nfdi4earth.de](mailto:helpdesk@nfdi4earth.de). We will coordinate follow-up activities with the related NFDI4Earth working groups.

![NFDI4Earth](img/NFDI4Earth.jpg)

<!--

If you contribute to the article, please don't forget to add you as author and to sign the author agreeement (https://nfdi4earth.pages.rwth-aachen.de/livinghandbook/livinghandbook/#LHB_author-guidelines_ENG/#author-agreement-and-licences).

-->
