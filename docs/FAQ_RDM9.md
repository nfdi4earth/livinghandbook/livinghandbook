---
name: How can I find a suitable repository for my data?

author: 
  - name: User Support Network

inLanguage: ENG

about:
  - internal nfdi4earth
  - dataset
  - sharing data
  - publication


isPartOf: 
  - FAQ.md 

additionalType: FAQ

subjectArea: 
  - unesco:concept3117 
  - unesco:concept7396

keywords:
  - data publication
  - data deposition

audience: 
  - data owner
  - data depositor

version: 1.0

license: CC-BY-4.0

---

# How can I find a suitable repository for my data?

Finding a suitable repository for your data involves considering various factors such as the type of data you have, the subject area of your research, and any specific requirements you need to meet (e.g., funder mandates, data sharing policies). A data repository search will be available from the OneStopPortal. You can also search [re3data](https://www.re3data.org/).

Remember that each repository may have its own submission and curation process, so it's essential to review the repository's guidelines and policies before depositing your data. Additionally, seeking advice from colleagues, research advisors, or data librarians can also be helpful in identifying the most suitable repository for your research data. 
