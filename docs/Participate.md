---
name: Participation & funding opportunities in the NFDI4Earth

author: 
  - name: NFDI4Earth Editorial Board
  #  orcidId: http://orcid.org/0000-0002-8186-3566

inLanguage: ENG

additionalType: collection

subjectArea: 
  - dfgfo:34
  - unesco:concept3621
  - unesco:concept5861
  - unesco:mt2.35

keywords:
  - NFDI4Earth

audience: 
  - data collector
  - data owner
  - data depositor
  - data user
  - data curator
  - data librarian
  - data steward
  - data advocate
  - data provider
  - service provider
  - research software engineer
  - policy maker
  - general public

version: 1.0

license: CC-BY-4.0

---

# Participation & funding opportunities in the NFDI4Earth

The NFDI4Earth engages with its community – be it student, professional, service provider, or scientist – through various participation and funding opportunites to foster collaboration, innovation, and skill development in Research Data Management (RDM) within Earth System Sciences (ESS): 

## Pilot Studies
Pilot Studies are projects funded for a period of up to one year with an E13 equivalent. Projects shall be aimed at testing, refining or implementing new RDM strategies, tools, or methodologies. Pilots allow researchers to experiment with innovative approaches before scaling up for broader implementation.

[Open calls and more information about pilots are found here.](https://www.nfdi4earth.de/2participate/pilots)

## Incubator Lab
The Incubator Lab projects are funded for 3-6 months. Incubators are high risk projects to explore novel data science developments and opportunities in the ESS, as well producing new infrastructure elements for the NFDI4Earth. Incubators also faciliate mentoring and networking opportunities to help researchers develop and refine their concepts into actionable projects. 

[Open calls and more information about incubators are found here.](https://www.nfdi4earth.de/?view=article&id=347&catid=15)

## Education and Training
Proposals for 3 months educational pilot projects can be submitted at any time. Education pilots are intended to develop high-quality and interactive educational materials required for the ESS community, which are then accessible through the NFDI4Earth EduPortal. 

[Find more information about Education & Training here.](https://www.nfdi4earth.de/2participate/education-training)

## Academy
Early Career Researchers (ECR) can apply to become a fellow with about 40 other fellows for two years the Academy program. The Academy runs a program for networking, training, and collaborative research of doctoral and postdoctoral researchers bridging Earth System and Data Science. Fellows benefit from tailored training courses such as summer schools, workshops, or seminars designed by peers and organized by the NFDI4Earth, as well as networking beyond institutional boundaries.

[Find more information about the Academy here.](https://www.nfdi4earth.de/2participate/academy)

## Interest Groups
Interest Groups (IGs) are temporary or permanent forums for a community or stakeholder group to identify, discuss and communicate specific RDM or related topics for the NFDI4Earth, jointly advance NFDI4Earth-related concepts, technologies and processes, implement NFDI4Earth principles, or todisseminate specific NFDI4Earth offers. 

[Find more information about Interest Groups here.](https://www.nfdi4earth.de/2participate/get-involved-by-interest-groups)
