---
name: 'PAMBase: Towards standardized soundscape recordings and analyses in Earth System Sciences'

description: Description of the NFDI4Earth Pilot project. 

author:
  - name: Kevin FA Darras
    orcidId: https://orcid.org/0000-0002-9013-3784
  - name: Frederik Pitz
    orcidId: https://orcid.org/0009-0002-8760-7771
  - name: Dánnell Quesada-Chacón
    orcidId: https://orcid.org/0000-0001-5700-1846
  - name: Felix Zichner
  - name: Paula Phan
  - name: Jan O. Engler
    orcidId: https://orcid.org/0000-0001-7092-1380
  - name: Anna F. Cord
    orcidId: https://orcid.org/0000-0003-3183-8482

inLanguage: ENG


about:
  - external nfdi4earth
  - tools and techniques


isPartOf: 
  - N4E_Pilots.md
  - Collection_ObserveCollectCreateEvaluate.md
  - Collection_SharePublish.md
  - Collection_ProcessAnalyse.md

additionalType: technical_note

subjectArea: 
  - dfgfo:315-01
  - dfgfo:203
  - dfgfo:317-02
  - unesco:concept122
  - unesco:concept8601
  - unesco:concept3277
  - unesco:concept160
  - unesco:concept522
  - unesco:concept17094

keywords:
  - data publication
  - passive acoustic monitoring
  - repository
  - deep learning

audience: 
  - general public
  - data curator
  - data user
  - data depositor

version: 1.0

identifier: https://doi.org/10.5281/zenodo.7785327


license: CC-BY-4.0

---

# PAMBase: Towards standardized soundscape recordings and analyses in Earth System Sciences

Passive acoustic monitoring is an effective method for tracking a variety of phenomena in the Earth System Sciences, whether they are devastating earthquakes (lithosphere), singing whales (biosphere) or human transportation (anthroposphere). However, we lack common databases and software tools for an integrated analysis of the acoustic information gathered across the different spheres. We aimed to deliver an operable software tool for inter-disciplinary acoustic analyses, and to engage the community around a roadmap for standardized workflows in passive acoustic monitoring. We present the front- and back-end of our tool based on an open-source ecoacoustics platform along with the functionality to allow the participation of the general public and the execution of deep learning algorithms to automatically identify bird species in sound recordings ([Media 1](#med1)). We also 1) surveyed user needs across the NFDI4Earth and NFDI4Biodiversity consortia; 2) present guidelines for data quality control, standardisation, and sharing; and 3) propose future directions for the technological and methodological development of common databases and software tools. Although we were not able to comprehensively assess or federate the Earth System Science community, we present clear improvements for the software-based acoustic data workflow used by the biogeographical research community.

<a id="med1"></a>

![Scheme illustrating the positioning of PAMbase (right) as a data repository for acoustic data (center) recorded from different realms of the Earth’s systems (left). A flow chart illustrates the three main work packages (WPs) and the information flows at the user and data level.](img/Pilot_PAMBase_media1.png "Scheme illustrating the positioning of PAMbase (right) as a data repository for acoustic data (center) recorded from different realms of the Earth’s systems (left). A flow chart illustrates the three main work packages (WPs) and the information flows at the user and data level.")

## Resources
* [PAMBase/EcoSound](https://ecosound-web.de/ecosound_web/)
* Roadmap / project report: ([@kevin_fa_darras_2023])
* Code Repository ([Github](https://github.com/ecomontec/ecoSound-web))
* Platform Documentation: ([@darras_f1000research])

## References