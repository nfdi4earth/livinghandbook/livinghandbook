---
name: Share and Publish

description: Collection of articles about research data management topics related to establishing and supporting the reach of published data and maximizing their impact. 

author: 
  - name: NFDI4Earth Editorial Board
#    orcidId: http://orcid.org/0000-0002-8186-3566

inLanguage: ENG

additionalType: collection

subjectArea: 
  - dfgfo:34
  - unesco:mt2.35
  - unesco:concept7396

keywords:
  - research data management
  - data sharing
  - data publication
  - reach

audience: 
  - data collector
  - data owner
  - data depositor
  - data user
  - data curator
  - data librarian
  - data steward
  - data advocate
  - data provider
  - service provider
  - research software engineer
  - policy maker
  - general public

version: 1.0

license: CC-BY-4.0

---

# Share, Manage, and Publish

## What is the share, manage and publish phase?

Research and research relevant data should be shared, managed and published open source whenever possible. Publishing research data in repositories is an efficient way to [archive](Collection_Archive.md) data. Share, manage, and publish data is the 7th phase of the data lifecycle. Here we provide options on how to make data accessible to the ESS community and ensure that research findings are disseminated effectively. 

**Share**: Research data should be stored in domain specific repositories, such as  [PANGAEA](https://pangaea.de/), [EarthChem](https://www.earthchem.org), or [GEOROC](https://georoc.mpch-mainz.gwdg.de/georoc/),  and research software should be stored in e.g., git-repositories. Journals might have specific requirements for data storage, or even lists of approved repositories where data needs to be permanently saved before a manuscript can be accepted. The data transferred to a repository need to be prepared following the FAIR principles, i.e., domain specific metadata or vocabularies as provided by e.g., [GeoSciML](http://geosciml.org/doc/geosciml/4.0/documentation/html/) or [ARDC](https://ardc.edu.au/).

**Manage**: This includes policies, procedures, and tools to consider and implement data protection laws, intellectual property rights, data sharing agreements, or memorandums. Privacy concerns are not a common issue in ESS, but can occur to e.g., protect valuable or rare sample sites or in surveys.

**Publish**: Publishing integrates datasets alongside scholarly articles or as independent datasets with their own Digital Object Identifier (DOIs) for citability and facilitating their use in future research projects. This process promotes transparency and reproducibility by making data accessible for verification and validation by peers. Proper [data licences](https://dmeg.cessda.eu/Data-Management-Expert-Guide/6.-Archive-Publish/Publishing-with-CESSDA-archives/Licensing-your-data) need to be applied. [PANGAEA](https://pangaea.de/), [re3data](https://www.re3data.org/), [GEOROC](https://georoc.mpch-mainz.gwdg.de/georoc/),  [EARTHCHEM](https://www.earthchem.org/) and [GFZ Data Services](https://dataservices-cms.gfz-potsdam.de/) are examples of data repository publishers for the Earth System Sciences.

## Why is the share, manage, and publish phase important?

This phase addresses the F (findable) and A (accessible) of the FAIR principles. Only the careful selection of an appropriate repository or database makes datasets fully available to the ESS community. Data sharing benefits researchers the ESS community, as well as the public. It encourages establishing connections between fields and collaboration between scientists from different disciplines, e.g., connecting data from rocks, soils, agriculture, climate, or urban development, as well as from different timescales, e.g., using time series of ancient and present climate data. Datasets can be, thus, effectively [accessed and reused](Collection_AccessReuse.md).

Storage in a data repository ensures immediate access to data. In contrast, long-term archiving data as done in the [Archive](Collection_Archive.md) phase (the 6th phase of the data lifecycle) provides only limited or infrequent access, prioritizing data durability over immediate usability.

Effective share and publish practices contribute to advancing scientific knowledge and addressing global environmental challenges by fostering collaboration and facilitating data-driven insights and innovations. Data sharing, publishing, and managing should also adhere to ethical guidelines, e.g., [CARE](WhoOwnsTheData_ENG.md), ensuring the responsible and ethical use of data while supporting the [FAIR](FAQ_RDM2.md) principles to enhance data discoverability and reuse.

## What will be offered in this article collection?

This collection covers best practices for data sharing, managing, and publishing, including selecting appropriate platforms for data sharing, preparing data for publication, and managing access and permissions. The collection provides practical examples and guidelines for ESS datasets to help researchers effectively share and disseminate their work. 

We encourage contributions from the community to help refine and enhance this collection and provide challenges and experience in data sharing, managing and publishing for more effective and collaborative data solutions.  

For more details on how to contribute, please refer to our [template article](example_ENG.md).