---
name: Archive

description: Collection of articles about research data management topics related to the archiving of research data. 

author: 
  - name: NFDI4Earth Editorial Board
 #   orcidId: http://orcid.org/0000-0002-8186-3566

inLanguage: ENG

additionalType: collection

subjectArea: 
  - dfgfo:34
  - unesco:mt2.35
  - unesco:concept522
  - unesco:concept2780
  - unesco:concept2768
  - unesco:concept2737

keywords:
  - research data management
  - data archiving
  - long-term archiving
  - data disposal

audience: 
  - data collector
  - data owner
  - data depositor
  - data user
  - data curator
  - data librarian
  - data steward
  - data advocate
  - data provider
  - service provider
  - research software engineer
  - policy maker
  - general public

version: 1.0

license: CC-BY-4.0

---

# Archive

## What is the archive phase?

Over time, most data is less frequently accessed, but remains important for reference, compliance, or new research questions. As the 6th phase of the data lifecycle, archiving involves moving ESS data to dedicated, ideally ESS domain specific long-term storage solutions.

**Archive**: The aim of archiving is to implement strategies for the long run. Appropriate data file formats, e.g., CSV, NetCDF or GeoTIFF, metadata standards such as ISO 19115 for geospatial data, and permissions are important factors. Archiving is a version of [Store and Organise](Collection_StoreManage.md), with its own specific requirements for sustainability and reuse for data preservation, with then often limited access towards data. [Publishing to data repositories](Collection_SharePublish.md) is a common way to archive data in the Earth System Sciences.

**Long-term archiving**: This is also called long-term preservation and goes one step further, referring to the secure storage of data over very long periods of time, often several decades or even centuries. Long-term archiving requires even more sensible and careful planning, including data migration,  updates to highly generic formats, and inclusion of related software in e.g., containers. For example, old seismic data stored on magnetic tapes need to be migrated to more standardised formats like SEG-Y. 

## Why is the archive phase important?

Data archiving includes updates to new formats and storage media with time, ensuring that data remains accessible as technology evolves. Geoscience often deals with datasets spanning long time periods, such as climate records, geological surveys, or paleoclimate data. Archiving ensures that such historical information is preserved for future research and reference. Access to archived datasets also supports interdisciplinary studies within the Earth System Science (ESS). Depending on permissions, datasets could be privately archived or [shared and published](Collection_SharePublish.md) with the ESS community.


## What will be offered in this article collection?

This collection explores the principles and practices of data archiving, focusing on long-term preservation strategies in ESS, including e.g., containerization. It includes guidance on selecting domain specific archives, archival formats, and metadata standards suitable for long-term storage. 

We encourage contributions from the community to help refine and enhance this collection and provide archiving strategies for more effective and collaborative data solutions.  

For more details on how to contribute, please refer to our [template article](example_ENG.md).