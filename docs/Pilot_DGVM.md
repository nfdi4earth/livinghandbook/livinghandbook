---
name: Optimizing global vegetation model with model-data integration approach

description: Description of the NFDI4Earth Pilot project. 

author: 
  - name: Naixin Fan
  - name: Matthias Forkel

inLanguage: ENG

about:
  - external nfdi4earth
  - data model
  - tools and techniques


isPartOf: 
  - N4E_Pilots.md
  - Collection_ObserveCollectCreateEvaluate.md

additionalType: technical_note

subjectArea: 
  - dfgfo:202-02
  - unesco:concept4700
  - unesco:concept7121

keywords:
  - dynamic processes
  - model-data integration
  - dynamic global vegetation models

audience: 
  - data collector
  - data user

identifier: https://doi.org/10.5281/zenodo.8131542


version: 1.0

license: CC-BY-4.0

---

# Optimizing global vegetation model with model-data integration approach

We present a model-data integration (MDI) framework for optimizing global vegetation model in this report ([Media 1](#med1)). Dynamic global vegetation models (DGVMs) are important tools to understand the trajectories of the terrestrial ecosystems and carbon cycle in response to the future climate change. However, the simulations of dynamical processes of vegetation can well be biased by the empirical or even arbitrary parameterization in the model. Our goal is to build a generic framework of gauging the parameters in the model with observational data to achieve a better representation of vegetation and its response to climate. We here show a case study in which the global dynamic vegetation model LPJmL (Lund-Potsdam-Jena managed Land) is optimized with observational site-level carbon flux and phenological datasets. We show a substantial reduction of bias and variabilities in the simulation of vegetation phenology and productivity after the parameters are optimized with the data-model integration approach. This is a proof of concept that this approach can be theoretically applied to improve the performance of other DGVMs. 

We implemented the MDI framework as a package for the R language. The observational data is used to reduce the distance between model output and observations by changing relevant parameters iteratively.

<a id="med1"></a>

![Schematic flowchart showing the model-data integration framework](img/Pilot_DGVM.png "A schematic flowchart showing the model-data integration framework.")

## Resources 
* Pilot report ([@fan_2023])

## References

