---
name: Rescued dataset of Monthly Tropical Stratospheric Zonal Winds from Radiosondes

description: Continuing an important dataset on stratospheric winds for atmospheric research in a FAIR way

author: 
  - name: Tobias Kerzenmacher
    orcidId: https://orcid.org/0000-0001-8413-0539
  - name: Peter Braesicke
    orcidId: https://orcid.org/0000-0003-1423-0619


inLanguage: ENG

about:
  - external nfdi4earth
  - dataset
  - tools and techniques


isPartOf: 
  - Collection_SharePublish.md
  - Collection_tools_pilots_incubators.md

subjectArea: 
  - dfgfo:313-02
  - unesco:concept160
  - unesco:mt2.35


keywords:
  - QBO
  - stratospheric winds


audience: 
  - data user


identifier: https://doi.org/10.5281/zenodo.10963871

version: 0.1

identifier: 


license: CC-BY-4.0

---


# Rescued dataset of Monthly Tropical Stratospheric Zonal Winds from Radiosondes


_**Contributions to the community**: The authors undertook great effort to make long-term data on Quasi-biennial Oscillation data (QBO) of stratospheric winds accessible and referencable. Now the data is provided in a FAIR way to the atmospheric chemistry and dynamics community._


Are you curious about the winds swirling high above the equator? We revived this dataset that lay dormant for just over one year, because the project was stopped at the Free University of Berlin who helped us to get it going again. It offers a unique glimpse into the monthly mean stratospheric zonal winds measured by radiosondes since 1953. Explore how these winds oscillate between easterlies and westerlies quasi every two years, a phenomenon known as the Quasi-Biennial Oscillation (QBO).


Monthly mean zonal wind data for the tropics are provided as a service for the global QBO and trend analysis communities. The original data source and processing chain were established by the Free University of Berlin (FUB). Currently, the data is processed at the Karlsruhe Institute of Technology (KIT, ROR:04t3en479), Institute of Meteorology and Climate Research (IMK), Germany with the tools developed at FUB.

Image: ![One decade of zonal winds in Singapore illustrating the fragile nature of the QBO that showed a regular quasi-biennial rhythm from the time of its discovery until winter 2015/2016.](img/QBO_singaporeRS_QBO_2013-2022.png "One decade of zonal winds in Singapore illustrating the fragile nature of the QBO that showed a regular quasi-biennial rhythm from the time of its discovery until winter 2015/2016.")

<p>&nbsp;</p>

## Detailed List of Radiosonde Stations:
The dataset includes monthly mean zonal wind values at pressure levels 100, 90, 80, 70, 60, 50, 45, 40, 35, 30, 25, 20, 15, 12, and 10 hPa, derived from radiosonde observations at four equatorial stations:

-	Kiribati (Canton Island) - data from 1953 to 1967 (closed)
-	Maldives (Gan Island) - data from 1967 to 1975 (closed)
-	Singapore (Payalebar) - data from 1975 to 1989
-	Singapore (Changi) - data from 1989 onwards

## Important Notes

-	Values for 100 hPa from October 1967 to December 1996 are solely from Singapore (Changi).
-	Values for 100 hPa before October 1967 are from Kiribati (Canton Island) when available.

## Outcomes
The authors undertook a great effort to make long-term data on Quasi-biennial Oscillation data (QBO) of stratospheric winds accessible and referenceable. Now, the data is provided FAIR to the atmospheric chemistry and dynamics community.

Data is publicly available and can be downloaded as 
- NetCDF [@kerzenmacher2024], or as 
- [(zipped) DAT files](https://www.atmohub.kit.edu/english/807.php) 

## Reference