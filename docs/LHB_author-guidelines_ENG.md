---
name: Publish your knowledge as Living Handbook article
description: Author guidelines for the NFDI4Earth Living Handbook.

author: 
  - name: Thomas Rose
    orcidId: http://orcid.org/0000-0002-8186-3566

inLanguage: ENG

about:
  - internal nfdi4earth
  - data service

isPartOf: 
  - LHB_ENG.md

additionalType: call_participate

subjectArea: 
  - unesco:concept3810
  - unesco:concept326
  - unesco:concept4224

keywords:
  - Living Handbook 
  - NFDI4Earth

audience: 
  - general public

version: 1.0

license: CC-BY-4.0

---

# Publish your knowledge as Living Handbook article

The NFDI4Earth Living Handbook provides (i) an encyclopaedia-style documentation for all aspects related to the NFDI4Earth, its services and outcomes in a human-readable form, and (ii) aims to structure and harmonise all information related to data management and data science approaches in the Earth System Sciences in a community-driven effort. These guidelines outline all important information for the preparation and submission of articles for the Living Handbook. They are continuously updated as the Living Handbook evolves. The [editorial workflow](LHB_editorial-workflow_ENG.md) describes how articles are processed after submission to the NFDI4Earth Living handbook. 

## Submission

Articles can   be submitted to the Living Handbook by mail via <incoming+nfdi4earth-livinghandbook-livinghandbook-79252-issue-@mail.git.rwth-aachen.de>. 

## Content
Articles in the Living Handbook summarise knowledge and are typically not original research. However, its content must be verifiable by providing reliable sources. These are ideally cited in line and listed at the end of the article. 

When writing an article, please use the "spiral out" approach as much as possible. "Spiral out" means:

-   start with the core of a topic
-   develop *all* its main aspects briefly
-   cover some or all of these main aspects in detail

This concept allows readers unfamiliar with the content to acquire general knowledge first, before engaging with the expert knowledge in the article. Moreover, it makes it easy for the reader to stop reading after they have gathered sufficient information. 

There is no minimum or maximum length for articles. We typically aim for an article length between 500 and 5000 words. It increases the readability of a complex topic if it is split into multiple articles rather than cramming all knowledge into one article. If in doubt, it is recommended to create multiple articles and bundle these as a [collection](LHB_author-guidelines_ENG.md#collections).

Any element other than article text, info-boxes, or content directly embedded into the text (e.g., code snippets or formulas) must have a caption. Elements embedded in the text must be referred to in the text.

Linking between articles is explicitly encouraged. Links between articles are written as `[Link text](article.md)`. To link to specific sections within an article, write `[Link text](article.md#section)`. The same syntax is used to link to external pages and sections within them by replacing `article.md` with the full URL of the page. Whenever possible, we prefer links that are permanent or likely to be long-lasting.  

## References
References to reliable sources must be provided and the location of their citations in the text indicated. Only references mentioned in the text should be included. If you include references, the article must end with `## References` followed by the bibtex-formatted references (if not provided in a separate file). 

If the cited source has a DOI, providing only the DOI is sufficient. However, it is strongly recommended to provide the full reference in case the DOI fails to resolve at the end of the manuscript or in a separate file. References in [bibtex format](https://www.bibtex.com/) are preferred. Bibliographic information in bibtex format can be provided in any order, there is no need to sort the references according to e.g., the alphabet. 

Bibtex is a widely used standard to provide bibliographic information in a way that allows uniform machine-operated formatting of references. Citations of resources can nowadays be downloaded in bibtex format from most of the publisher webpages in the "Cite as" sections. In addition, the two major DOI-minting organisations Crossref (for publications) and Datacite (for datasets, software and other media types) provide ways to directly retrieve the bibtex-formatted bibliographic information of a DOI record:

* Crossref: `https://api.crossref.org/works/<DOI>/transform/application/x-bibtex` (e.g., <https://api.crossref.org/works/10.5555/12345678/transform/application/x-bibtex>)
* Datacite: `https://api.datacite.org/application/x-bibtex/<DOI>` (e.g., <https://api.datacite.org/application/x-bibtex/10.5281/zenodo.5718943>)

Moreover, web tools exist that create bibtex-formatted references for webpages and other media, such as <https://www.bibme.org/bibtex/website-citation>. 

Citations in the text must be provided in a way that allows to easily identify their entry in the reference section. If possible, please indicate them by `[@KEY]` with KEY being a string that identifies the respective reference (e.g., its DOI or something like "Smith_et_al2022"; in bibtex formatted references the content of the first line after `{`). A semicolon separates multiple references: `[@Doe2020; @Smith_et_al2022]`. 

In addition to the references in the text, you are encouraged to provide an "Additional resources" section of your manuscript that helps readers to dive deeper into the topic. 

## Collections
Collections bundle topically related articles. A collection has the formal structure of a regular article that starts with a brief introduction text to the topic of the collection, followed by a list of links to the included articles. You may create synergies by suggesting an appropriate topical collection in which your article could be included alongside other articles. You can learn more about collections in the [collection article](LHB_collections.md).

## Interactive content
In principle, interactive content can be included in the NFDI4Earth Living Handbook as can be any kind of media. Implementation of different interactive content types will be done on a case-by-case basis. Please get in touch with the editorial board via <nfdi4earth-livinghandbook@tu-dresden.de> if you plan to include interactive content. 

## Author agreement and licences
To contribute to the NFDI4Earth Living Handbook, you must accept our author agreement (read [it](LHB_author-agreement_ENG.md) ). The managing editor will ask you to do so upon submission of the article, by inviting you to subscribe to the mailing list <nfdi4earth-lhb-contributors@groups.tu-dresden.de> (we use this approach as a GDPR-compliant way for storing this information). Confirming the invitation is regarded as equivalent to accepting the author agreement. No mails will be sent through this mailing list unless we have to notify you about any circumstances affecting the author agreement. It is also possible to accept the author's agreement proactively by [subscribing to the mailing list](https://mailman.zih.tu-dresden.de/groups/listinfo/nfdi4earth-lhb-contributors).  

The article and all its original content, including any additional files such as images, must be made available under a [CC-BY 4.0](https://creativecommons.org/licenses/by/4.0/) licence.  

Linked content, such as videos or external figures, must be referenced in the caption according to their license. In any case, the reference must contain at least the author, a link to the webpage the work is taken from, and the name of the licence under which the work was published. The name of the licence shall be a link to the text of the respective licence. 

The author declares that all material used in the article, including external materials, is suitably licensed or can be published under them. The author further declares that appropriate credit is given to all sources used for the article and that any parts copied from other sources are marked as quotes. 

## Accessibility
The following points should be considered to make the content of articles as accessible as possible: 

- Provision of an alternative text to any non-text element such as figures or charts. The alternative text describes this element. 
- Use of colour-blind friendly colour schemes. See e.g. <https://davidmathlogic.com/colorblind> for basic guidance on colouring for colorblindness, and <https://www.color-blindness.com/coblis-color-blindness-simulator/> to check how your figure might look in different types of colour blindness. The tool <https://colorbrewer2.org> is a great resource for different color schemes, including colour-blind friendly ones. 
- Ensure high contrast of graphical elements
- Clear and coherent structure of the article 

There are numerous pages available to learn more about how to make a web-based article accessible. A good starting point might be the [page of the W3C about this topic](https://www.w3.org/WAI/fundamentals/accessibility-intro/). 

## Inclusivity
Having a wide range of readers with a vast variety of backgrounds, the NFDI4Earth Living Handbook aims to be as inclusive as possible. This also concerns its content. There are numerous guides available in the internet on how to write articles in inclusive language. 

[Promoting inclusive inLanguage: an incomplete guide (v2)](https://blogs.egu.eu/geolog/files/2021/01/Promoting-inclusive-language-v2.pdf) by the EGU provides a brief and concise introduction on this topic. In addition, the [Specific guidelines for English](https://www.europarl.europa.eu/cmsdata/151780/GNL_Guidelines_EN.pdf#page=10) in the Gender-Neutral Language Guidelines of the European Parliament provide additional tips and many examples. 

## Technical specifications
Articles should be submitted in markdown (`.md`) if possible. Otherwise, plain text (`.txt`) or formatted text (`.doc`, `.docx`) is also accepted. In formatted texts, formatting should be restricted to a minimum. Any images and other media must be provided as separate files. Please reach out to the editorial board via <nfdi4earth-livinghandbook@tu-dresden.de> if you need help with preparing your manuscript. 

Files other than the text should be submitted in the following formats: 

| Type | File type |
|-------|-------|
| Raster graphics | `.png` |
| Vector graphics | `.svg` |
| Diagrams, Flowcharts, ... | `.svg` or as [mermaid](https://mermaid.js.org/) code in the article. |

## Additional resources

* Template article ([md format](example_ENG.md) | [docx format](pdf/template_article_ENG.docx))
