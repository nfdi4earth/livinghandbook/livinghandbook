---
name: How do I cite research data correctly?

author: 
  - name: User Support Network

inLanguage: ENG

about:
  - internal nfdi4earth
  - publication
  - sharing data


isPartOf: 
  - FAQ.md 

additionalType: FAQ

subjectArea: 
  - unesco:concept3117 
  - unesco:concept7396

keywords:
  - data citation

audience: 
  - data collector
  - data user
  - data steward

version: 1.0

license: CC-BY-4.0

---

# How do I cite research data correctly?

Citing research data correctly is essential for giving credit to the data creators and making your research transparent and reproducible. When citing research data, you should include the necessary information to uniquely identify the dataset and provide enough detail for others to access it. Here are some general guidelines to help you cite research data correctly:

* Author/Creator: The individual(s) or organization responsible for creating the dataset. 
* Title: The title of the dataset. 
* Year: The year when the dataset was published or made available. 
* Publisher/Repository: The name of the data repository or publisher where the dataset is hosted. 
* Identifier: The unique identifier for the dataset, such as a Digital Object Identifier (DOI) or a persistent URL. If the dataset is publicly accessible, you can include the URL or a direct link to the dataset.

## Follow Repository Guidelines
Some data repositories have specific citation guidelines or provide a preferred citation format, such as [PANGAEA](https://wiki.pangaea.de/wiki/Citation). Always check the repository's website for respective instructions. If the dataset has multiple versions or editions, be sure to cite the specific version you used. Remember that accurate and complete citations not only give credit to the original data creators but also allow other researchers to find and use the same dataset to verify your research or conduct further analyses.

Never just mention the repository you plan to submit your data to, e.g. Data will be available at data.xxx.de! 

